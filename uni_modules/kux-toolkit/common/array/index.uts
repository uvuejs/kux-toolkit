/**
 * 将一个数组分割成指定长度的多个子数组
 * @description 函数接受一个数组和一个数字作为参数，返回一个新的数组，其中包含原数组分割成指定长度的多个子数组。
 * @param arr 要分割的数组
 * @param size 子数组的长度
 * @returns 一个新的数组，其中包含原数组分割成指定长度的多个子数组。
 */
export function chunk<T>(arr : T[], size : number) : T[][] {
	const result : T[][] = []
	for (let i = 0; i < arr.length; i += size) {
		result.push(arr.slice(i, i + size))
	}
	return result
}

/**
 * 将多个数组连接成一个数组
 * @description 函数接受多个数组作为参数，返回一个新的数组，其中包含所有传入的数组的元素。
 * @param arrs 要合并的数组
 * @returns 一个新的数组，其中包含所有传入的数组的元素。
 */
export function concat<T>(...arrs : T[][]) : T[] {
	let result : T[] = []

	for (let i = 0; i < arrs.length; i++) {
		result = result.concat(arrs[i])
	}

	return result
}

/**
 * 将多个值连接成一个数组
 * @description 函数接受多个值作为参数，返回一个新的数组，其中包含所有传入的值。
 * @param values 要合并的值
 * @returns 一个新的数组，其中包含所有传入的值。
 */
export function concatValues<T>(...values : T[]) : T[] {
	let result : T[] = []

	for (let i = 0; i < values.length; i++) {
		result.push(values[i])
	}

	return result
}

function getObjKeys(obj : UTSJSONObject) : string[] {
	// #ifdef WEB
	return Object.keys(obj)
	// #endif
	// #ifndef WEB
	return UTSJSONObject.keys(obj)
	// #endif
}
/**
 * 根据 `mapper` 函数统计数组中每个项目出现的次数
 * @description 函数接受一个数组和一个 `mapper` 函数作为参数，返回一个对象，其中包含数组中每个项目出现的次数。
 * @param arr 要统计的数组
 * @param mapper 一个函数，用于将数组中的每个元素转换成一个键值对
 * @returns 一个对象，其中包含数组中每个项目出现的次数。
 */
export function countBy<T>(arr : T[], mapper : (item : T) => string) : UTSJSONObject {
	const result : UTSJSONObject = {}
	let count = 0
	arr.forEach(item => {
		const key = mapper(item)
		if (getObjKeys(result).includes(key)) {
			let count = result.get(key) as number
			result.set(key, count + 1)
		} else {
			result.set(key, 1)
		}
	})
	return result
}

/**
 * 从数组中删除假值(`false`、`null`、`0`、`''`、`undefined`、`NaN`)
 * @description 函数接受一个数组作为参数，返回一个新的数组，其中不包含假值。
 * @param arr 要过滤的数组
 * @returns 一个新的数组，其中不包含假值。
 */
export function compact<T>(arr : T[]) : T[] {
	let result : T[] = []
	arr.forEach(item => {
		if (item != false && item != null && item != 0 && item != '') {
			result.push(item)
		}
	})
	return result
}

/**
 * 计算两个数组的差集
 * @description 函数接受两个数组作为参数，返回一个新的数组，其中包含第一个数组中不包含第二个数组中元素。
 * @param firstArr 第一个数组
 * @param secondArr 第二个数组
 * @returns 一个新的数组，其中包含第一个数组中不包含第二个数组中元素。
 */
export function difference<T>(firstArr : T[], secondArr : T[]) : T[] {
	const result : T[] = []
	firstArr.forEach(item => {
		if (!secondArr.includes(item)) {
			result.push(item)
		}
	})
	return result
}

/**
 * 计算经过提供的函数处理后的两个数组的差集
 * @description 函数接受两个数组和一个函数作为参数，返回一个新的数组，其中包含第一个数组中不包含第二个数组中元素，且经过提供的函数处理。
 * @param firstArr 第一个数组
 * @param secondArr 第二个数组
 * @param mapper 一个函数，用于映射两个数组元素的函数。该函数应用于两个数组中的每个元素，并基于映射后的值进行比较。
 */
export function differenceBy<T, U>(firstArr : T[], secondArr : T[], mapper : (item : T) => U) : T[] {
	const result : T[] = []
	firstArr.forEach(item => {
		const mappedItem = mapper(item)
		if (!secondArr.some((secondItem) : boolean => mappedItem == mapper(secondItem))) {
			result.push(item)
		}
	})
	return result
}

/**
 * 根据自定义相等函数计算两个数组的差集
 * @description 函数接受两个数组和一个自定义相等函数作为参数，返回一个新的数组，其中包含第一个数组中不包含第二个数组中元素，且经过提供的函数处理。
 * @param firstArr 第一个数组
 * @param secondArr 第二个数组
 * @param isEqual 一个函数，用于判断两个数组元素是否相等。该函数接受两个参数，并返回一个布尔值
 * @returns 一个新的数组，其中包含第一个数组中不包含第二个数组中元素，且经过提供的函数处理。
 */
export function differenceWith<T>(firstArr : T[], secondArr : T[], isEqual : (item1 : T, item2 : T) => boolean) : T[] {
	const result : T[] = []
	firstArr.forEach(item => {
		if (!secondArr.some((secondItem) : boolean => isEqual(item, secondItem))) {
			result.push(item)
		}
	})
	return result
}

/**
 * 从数组的开头移除指定数量的元素并返回剩余的元素
 * @description 函数接受一个数组和一个数字作为参数，返回一个新的数组，其中包含原数组从开头移除指定数量的元素后的元素。
 * @param arr 要过滤的数组
 * @param n 要移除的元素数量
 * @returns 一个新的数组，其中包含原数组从开头移除指定数量的元素后的元素。
 */
export function drop<T>(arr : T[], n : number) : T[] {
	return arr.slice(n)
}

/**
 * 从数组的开头移除元素，直到遇到第一个满足条件的元素
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含原数组从开头移除元素，直到遇到第一个满足条件的元素后的元素。
 * @param arr 要过滤的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。该函数接受一个参数，并返回一个布尔值
 * @returns 一个新的数组，其中包含原数组从开头移除元素，直到遇到第一个满足条件的元素后的元素。
 */
export function dropWhile<T>(arr : T[], predicate : (item : T) => boolean) : T[] {
	let i = 0
	while (i < arr.length && predicate(arr[i])) {
		i++
	}
	return arr.slice(i)
}

/**
 * 从数组的结尾移除指定数量的元素并返回剩余的元素
 * @description 函数接受一个数组和一个数字作为参数，返回一个新的数组，其中包含原数组从结尾移除指定数量的元素后的元素。
 * @param arr 要过滤的数组
 * @param n 要移除的元素数量
 * @returns 一个新的数组，其中包含原数组从结尾移除指定数量的元素后的元素。
 */
export function dropRight<T>(arr : T[], n : number) : T[] {
	return arr.slice(0, -n)
}

/**
 * 从数组的结尾移除元素，直到遇到第一个满足条件的元素
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含原数组从结尾移除元素，直到遇到第一个满足条件的元素后的元素。
 * @param arr 要过滤的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。该函数接受一个参数，并返回一个布尔值
 * @returns 一个新的数组，其中包含原数组从结尾移除元素，直到遇到第一个满足条件的元素后的元素。
 */
export function dropRightWhile<T>(arr : T[], predicate : (item : T) => boolean) : T[] {
	let i = arr.length - 1
	while (i >= 0 && predicate(arr[i])) {
		i--
	}
	return arr.slice(0, i + 1)
}

/**
 * 填充数组中从开始位置到结束位置（不包括结束位置）的元素为指定值
 * @description 函数接受一个数组、一个起始索引、一个结束索引和一个值作为参数，返回一个新的数组，其中包含原数组中从起始索引到结束索引（不包括结束索引）的元素填充为指定值后的元素。如果未指定起始索引或结束索引，则默认填充整个数组。还可以使用负数索引从数组末尾开始计数。
 * @param arr 要填充的数组
 * @param start 起始索引
 * @param end 结束索引
 * @param value 要填充的值
 */
export function toFilled<T, P>(arr : T[], value : P, start : number = 0, end : number = arr.length) : any[] {
	let _start = start
	let _end = end
	// 创建数组的副本
	const result = [...arr] as any[]

	// 处理负数索引
	if (start < 0) {
		_start = Math.max(0, arr.length + start)
	}
	if (end < 0) {
		_end = Math.max(0, arr.length + end)
	}
	// 处理索引越界
	_start = Math.min(arr.length, _start)
	_end = Math.min(arr.length, _end)

	// 填充数组
	for (let i = _start; i < _end; i++) {
		result[i] = value as any
	}

	return result
}

/**
 * 将嵌套数组的每个元素映射到给定的迭代函数，然后将其展开到所需的深度
 * @description 函数接受一个数组、一个迭代函数和一个深度作为参数，返回一个新的数组，其中包含原数组中每个元素经过迭代函数映射后的结果，并展开到所需的深度。
 * @param arr 要展开的数组
 * @param iteratee 一个迭代函数，用于将数组中的元素映射到另一个值。该函数接受一个参数，并返回一个值。
 * @param depth 要展开的深度。如果未指定，则默认为 1。
 * @returns 一个新的数组，其中包含原数组中每个元素经过迭代函数映射后的结果，并展开到所需的深度。
 */
export function flatMap<T, U>(arr : T[], iteratee : (item : T) => U[], depth : number = 1) : U[] {
	let result : U[] = [];
	// 定义一个递归函数处理深度展开
	function flatten(current : any, currentDepth : number) : void {
		// 如果当前深度为0或者当前项不是数组，直接添加到结果中
		if (currentDepth == 0 || !Array.isArray(current)) {
			result = result.concat(current);
			return;
		}

		(current as U[]).forEach((item) => {
			flatten(item as any, currentDepth - 1);
		});
	}

	arr.forEach((item) => {
		const mapped = iteratee(item);
		flatten(mapped, depth);
	});

	return result;
}

/**
 * 将数组展开到所需的深度
 * @description 函数接受一个数组和一个深度作为参数，返回一个新的数组，其中包含原数组中每个元素展开后的结果，并展开到所需的深度。
 * @param arr 要展开的数组
 * @param depth 要展开的深度。如果未指定，则默认为 1。
 * @returns 一个新的数组，其中包含原数组中每个元素展开后的结果，并展开到所需的深度。
 */
export function flatten<T>(arr : T[], depth : number = 1) : T[] {
	let result : T[] = [];
	// 定义一个递归函数处理深度展开
	function flatten(current : any, currentDepth : number) : void {
		// 如果当前深度为0或者当前项不是数组，直接添加到结果中
		if (currentDepth == 0 || !Array.isArray(current)) {
			result.push(current as T);
			return;
		}

		(current as T[]).forEach((item) => {
			flatten(item as any, currentDepth - 1);
		});
	}

	arr.forEach((item) => {
		flatten(item as any, depth);
	});

	return result;
}

/**
 * 一次性展开数组中的元素
 * @description 函数接受一个数组作为参数，返回一个新的数组，其中包含原数组中所有元素展开后的结果。
 * @param arr
 * @returns 一个新的数组，其中包含原数组中所有元素展开后的结果。
 */
export function flattenDeep<T>(arr : T[]) : T[] {
	return flatten(arr, Infinity)
}

/**
 * 从右到左迭代数组的每个元素，并对其执行指定函数
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含原数组中每个元素从右到左迭代后的结果。
 * @param arr 要迭代的数组
 * @param iteratee 一个函数，用于对数组中的元素进行处理。该函数接受一个参数，并返回一个值。
 * @returns 一个新的数组，其中包含原数组中每个元素从右到左迭代后的结果。
 */
export function forEachRight<T, U>(arr : T[], iteratee : (item : T, index : number, arr : T[]) => U) : U[] {
	for (let i = arr.length - 1; i >= 0; i--) {
		iteratee(arr[i], i, arr)
	}
	return arr as U[]
}

/**
 * 将数组的元素根据指定函数分组
 * @description 函数接受一个数组和一个函数作为参数，返回一个对象，其中包含原数组中每个元素根据指定函数分组后的结果。
 * @param arr 要分组的数组
 * @param iteratee 一个函数，用于将数组中的元素映射到另一个值。该函数接受一个参数，并返回一个值。
 * @returns 一个对象，其中包含原数组中每个元素根据指定函数分组后的结果。
 */
export function groupBy<T, U>(arr : T[], iteratee : (item : T) => U) : UTSJSONObject {
	const result : UTSJSONObject = {}
	arr.forEach(item => {
		const key = iteratee(item)
		if (getObjKeys(result).includes(key as string)) {
			(result.get(key) as T[]).push(item)
		} else {
			result.set(key, [item])
		}
	})
	return result
}

/**
 * 计算两个数组的交集
 * @description 函数接受两个数组作为参数，返回一个新的数组，其中包含两个数组的交集。
 * @param firstArr 第一个数组
 * @param secondArr 第二个数组
 * @returns 一个新的数组，其中包含两个数组的交集。
 */
export function intersection<T>(firstArr : T[], secondArr : T[]) : T[] {
	const result : T[] = []
	firstArr.forEach(item => {
		if (secondArr.includes(item)) {
			result.push(item)
		}
	})
	return result
}


/**
 * 计算经过提供的函数处理后的两个数组的交集
 * @description 函数接受两个数组和一个函数作为参数，返回一个新的数组，其中包含两个数组经过提供的函数处理后的交集。
 * @param firstArr 第一个数组
 * @param secondArr 第二个数组
 * @param mapper 一个函数，用于映射两个数组元素的函数。该函数应用于两个数组中的每个元素，并基于映射后的值进行比较。
 * @returns 一个新的数组，其中包含两个数组经过提供的函数处理后的交集。
 */
export function intersectionBy<T, U>(firstArr : T[], secondArr : T[], mapper : (item : T) => U) : T[] {
	const result : T[] = []
	firstArr.forEach(item => {
		const mappedItem = mapper(item)
		if (secondArr.some((secondItem) : boolean => mappedItem == mapper(secondItem))) {
			result.push(item)
		}
	})
	return result
}

/**
 * 根据自定义相等函数计算两个数组的交集
 * @description 函数接受两个数组和一个自定义相等函数作为参数，返回一个新的数组，其中包含两个数组经过提供的函数处理后的交集。
 * @param firstArr 第一个数组	
 * @param secondArr 第二个数组
 * @param isEqual 一个函数，用于判断两个数组元素是否相等。该函数接受两个参数，并返回一个布尔值
 * @returns 一个新的数组，其中包含两个数组经过提供的函数处理后的交集。
 */
export function intersectionWith<T>(firstArr : T[], secondArr : T[], isEqual : (item1 : T, item2 : T) => boolean) : T[] {
	const result : T[] = []
	firstArr.forEach(item => {
		if (secondArr.some((secondItem) : boolean => isEqual(item, secondItem))) {
			result.push(item)
		}
	})
	return result
}

/**
 * 映射数组中的每个元素，基于提供的生成键的函数
 * @description 函数接受一个数组和一个函数作为参数，返回一个对象，其中包含原数组中每个元素映射后的结果，基于提供的生成键的函数。
 * @param arr 要映射的数组
 * @param iteratee 一个函数，用于将数组中的元素映射到另一个值。该函数接受一个参数，并返回一个值。
 * @returns 一个对象，其中包含原数组中每个元素映射后的结果，基于提供的生成键的函数。
 */
export function keyBy<T, U>(arr : T[], iteratee : (item : T) => U) : UTSJSONObject {
	const result : UTSJSONObject = {}
	arr.forEach(item => {
		const key = iteratee(item)
		result[key as string] = item
	})
	return result
}

/**
 * 找到数组中通过将 `getValue` 函数应用于每个元素来返回具有最小值的元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @description 函数接受一个数组和一个函数作为参数，返回数组中通过将 `getValue` 函数应用于每个元素来返回具有最小值的元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @param arr 要查找的数组
 * @param getValue 一个函数，用于从数组元素中提取一个值。该函数接受一个参数，并返回一个值
 * @returns 数组中通过将 `getValue` 函数应用于每个元素来返回具有最小值的元素，并返回该元素的值。
 */
export function minBy<T>(arr : T[], getValue : (item : T) => number) : T | null {
	let result : T | null = null
	let minValue = Infinity
	arr.forEach(item => {
		const value = getValue(item)
		if (value < minValue) {
			result = item
			// #ifdef APP-ANDROID
			minValue = value.toDouble()
			// #endif
			// #ifndef APP-ANDROID
			minValue = value
			// #endif
		}
	})
	return result
}

/**
 * 找到数组中通过将 `getValue` 函数应用于每个元素来返回具有最大值的元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @description 函数接受一个数组和一个函数作为参数，返回数组中通过将 `getValue` 函数应用于每个元素来返回具有最大值的元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @param arr 要查找的数组
 * @param getValue 一个函数，用于从数组元素中提取一个值。该函数接受一个参数，并返回一个值
 * @returns 数组中通过将 `getValue` 函数应用于每个元素来返回具有最大值的元素，并返回该元素的值。
 */
export function maxBy<T>(arr : T[], getValue : (item : T) => number) : T | null {
	let result : T | null = null
	let maxValue = -Infinity
	arr.forEach(item => {
		const value = getValue(item)
		if (value > maxValue) {
			result = item
			// #ifdef APP-ANDROID
			maxValue = value.toDouble()
			// #endif
			// #ifndef APP-ANDROID
			maxValue = value
			// #endif
		}
	})
	return result
}

/**
 * 找到数组中的最小元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @description 函数接受一个数组作为参数，返回数组中的最小元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @param arr 要查找的数组
 * @returns 数组中的最小元素，并返回该元素的值。
 */
export function min<T>(arr : T[]) : T | null {
	// 处理空数组
	if (arr.length == 0) {
		return null
	}

	// 找到最小值
	return arr.reduce((prev, curr) : T => {
		// 判断是否为数字类型，如果是，则进行比较
		if (typeof prev == 'number' && typeof curr == 'number') {
			return ((prev != null && (prev as number) < (curr as number)) ? prev : curr) as T;
		}
		// 判断是否为字符串类型，如果是，则获取ASCII码进行比较
		if (typeof prev == 'string' && typeof curr == 'string') {
			const prevCode = (prev as string).charCodeAt(0)
			const currCode = (curr as string).charCodeAt(0)
			if (prevCode != null && currCode != null) {
				return ((prevCode < currCode) ? prev : curr) as T;
			}
		}
		// 其他类型，直接返回
		return prev as T
	}, arr[0])
}

/**
 * 元素大小比较，自动判断类型并进行比较
 */
function compare(a : any, b : any) : number {
	// 判断是否为数字类型，如果是，则进行比较
	if (typeof a == 'number' && typeof b == 'number') {
		return ((a != null && (a as number) < (b as number)) ? -1 : ((a != null && (a as number) > (b as number)) ? 1 : 0)) as number;
	}
	// 判断是否为字符串类型，如果是，则获取ASCII码进行比较
	if (typeof a == 'string' && typeof b == 'string') {
		const aCode = (a as string).charCodeAt(0)
		const bCode = (b as string).charCodeAt(0)
		if (aCode != null && bCode != null) {
			return ((aCode < bCode) ? -1 : ((aCode > bCode) ? 1 : 0)) as number;
		}
	}
	// 其他类型，直接返回
	return 0
}

/**
 * 找到数组中的最大元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @description 函数接受一个数组作为参数，返回数组中的最大元素，并返回该元素的值。如果数组为空，则返回 `null`。
 * @param arr 要查找的数组
 * @returns 数组中的最大元素，并返回该元素的值。
 */
export function max<T>(arr : T[]) : T | null {
	// 处理空数组
	if (arr.length == 0) {
		return null
	}

	// 找到最大值
	return arr.reduce((prev, curr) : T => {
		// 判断是否为数字类型，如果是，则进行比较
		if (typeof prev == 'number' && typeof curr == 'number') {
			return ((prev != null && (prev as number) > (curr as number)) ? prev : curr) as T;
		}
		// 判断是否为字符串类型，如果是，则获取ASCII码进行比较
		if (typeof prev == 'string' && typeof curr == 'string') {
			const prevCode = (prev as string).charCodeAt(0)
			const currCode = (curr as string).charCodeAt(0)
			if (prevCode != null && currCode != null) {
				return ((prevCode > currCode) ? prev : curr) as T;
			}
		}
		// 其他类型，直接返回
		return prev as T
	}, arr[0])
}

/**
 * 根据多个属性和它们对应的排序顺序对对象数组进行排序
 * @description 函数接受一个对象数组，一个要排序的属性数组，以及一个排序顺序数组作为参数，返回一个排序后的对象数组，根据每个键及其排序顺序进行排序。(`asc` 表示升序，`desc` 表示降序) 如果一个键的值相等，则按照下一个键进行排序，以此类推。
 * @param arr 要排序的对象数组
 * @param props 要排序的属性数组
 * @param orders 排序顺序数组
 * @returns 排序后的对象数组
 */
export function orderBy<T>(arr : T[], props : string[], orders : ('asc' | 'desc')[]) : T[] {
	const result : T[] = [...arr]
	result.sort((a, b) : number => {
		for (let i = 0; i < props.length; i++) {
			const prop = props[i]
			const order = orders[i]
			const value1 = (a as UTSJSONObject)[prop]
			const value2 = (b as UTSJSONObject)[prop]
			if (compare(value1 as any, value2 as any) == -1) {
				return order == 'asc' ? -1 : 1
			} else if (compare(value1 as any, value2 as any) == 1) {
				return order == 'asc' ? 1 : -1
			}
		}
		return 0
	})
	return result
}

/**
 * 返回数组中的随机元素
 * @description 函数接受一个数组作为参数，返回数组中的随机元素。空数组返回 `null`。
 * @param arr 要查找的数组
 * @returns 数组中的随机元素。
 */
export function sample<T>(arr : T[]) : T | null {
	return arr.length > 0 ? arr[Math.floor(Math.random() * arr.length)] : null
}

/**
 * 从数组中随机选取指定数量的元素
 * @description 函数接受一个数组和一个数字作为参数，返回一个新的数组，其中包含从数组中随机选取指定数量的元素。
 * @param arr 要查找的数组
 * @param n 要选取的元素数量
 * @returns 一个新的数组，其中包含从数组中随机选取指定数量的元素。
 */
export function sampleSize<T>(arr : T[], n : number) : T[] {
	const length = arr.length
	return length > 0 ? arr.sort((_a, _b) : number => Math.random() - 0.5).slice(0, Math.max(0, Math.min(n, length))) : [] as T[]
}

/**
 * 随机化数组中元素的顺序，使用 Fisher-Yates 算法
 * @description 函数接受一个数组作为参数，返回一个新的数组，其中包含原数组随机化顺序后的元素。
 * @param arr 要随机化的数组
 * @returns 一个新的数组，其中包含原数组随机化顺序后的元素。
 */
export function shuffle<T>(arr : T[]) : T[] {
	for (let i = arr.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1))
		const temp = arr[i]
		arr[i] = arr[j]
		arr[j] = temp
	}
	return arr
}

/**
 * 返回数组、对象或字符串的长度
 * @description 函数接受一个数组、对象或字符串作为参数，返回其长度。
 * @param value 要查找的数组、对象或字符串
 * @returns 数组、对象或字符串的长度。
 */
export function size(value : any) : number {
	// 处理数组
	if (Array.isArray(value)) {
		return (value as any[]).length
	}
	// 处理对象
	if (typeof value == 'object') {
		return getObjKeys(value as UTSJSONObject).length
	}
	// 处理字符串
	if (typeof value == 'string') {
		return (value as string).length
	}
	return 0
}

/**
 * 返回一个数组，其中包含从输入数组 `arr` 中获取的前 `n` 个元素
 * @description 函数接受一个数组和一个数字作为参数，返回一个新的数组，其中包含从输入数组 `arr` 中获取的前 `n` 个元素。
 * @param arr 要查找的数组
 * @param n 要获取的元素数量
 * @returns 一个新的数组，其中包含从输入数组 `arr` 中获取的前 `n` 个元素
 */
export function take<T>(arr : T[], n : number) : T[] {
	return arr.slice(0, Math.max(0, Math.min(n, arr.length)))
}

/**
 * 返回一个数组，其中包含满足提供的谓词函数的前导元素
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含满足提供的谓词函数的前导元素。
 * @param arr 要查找的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。
 * @returns 一个新的数组，其中包含满足提供的谓词函数的前导元素。
 */
export function takeWhile<T>(arr : T[], predicate : (item : T) => boolean) : T[] {
	const result : T[] = []
	for (let i = 0; i < arr.length; i++) {
		if (predicate(arr[i])) {
			result.push(arr[i])
		} else {
			break
		}
	}
	return result
}

/**
 * 返回一个数组，其中包含从输入数组 `arr` 中获取的最后 `n` 个元素
 * @description 函数接受一个数组和一个数字作为参数，返回一个新的数组，其中包含从输入数组 `arr` 中获取的最后 `n` 个元素。
 * @param arr 要查找的数组
 * @param n 要获取的元素数量
 * @returns 一个新的数组，其中包含从输入数组 `arr` 中获取的最后 `n` 个元素
 */
export function takeRight<T>(arr : T[], n : number) : T[] {
	return arr.slice(Math.max(0, arr.length - Math.max(0, Math.min(n, arr.length))))
}

/**
 * 返回一个数组，其中包含从输入数组 `arr` 中获取的最后 `n` 个元素，直到遇到第一个不满足条件的元素
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含从输入数组 `arr` 中获取的最后 `n` 个元素，直到遇到第一个不满足条件的元素。
 * @param arr 要查找的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。
 * @param n 要获取的元素数量
 * @returns 一个新的数组，其中包含从输入数组 `arr` 中获取的最后 `n` 个元素，直到遇到第一个不满足条件的元素
 */
export function takeRightWhile<T>(arr : T[], predicate : (item : T) => boolean) : T[] {
	const result : T[] = []
	for (let i = arr.length - 1; i >= 0; i--) {
		if (predicate(arr[i])) {
			result.unshift(arr[i])
		} else {
			break
		}
	}
	return result
}

/**
 * 返回两个数组的并集
 * @description 函数接受两个数组作为参数，返回一个新的数组，其中包含两个数组的并集。
 * @param arr1 第一个数组
 * @param arr2 第二个数组
 * @returns 一个新的数组，其中包含两个数组的并集。
 */
export function union<T>(arr1 : T[], arr2 : T[]) : T[] {
	return [...new Set([...arr1, ...arr2])]
}

/**
 * 使用提供的映射函数来确定相等性，从所有给定的数组中创建一个按顺序的唯一值数组。
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含两个数组的交集，使用提供的映射函数来确定相等性。
 * @param arr1 第一个数组
 * @param arr2 第二个数组
 * @param mapper 一个函数，用于处理每个元素。该函数接受一个参数，并返回一个值
 * @returns 一个新的数组，其中包含两个数组的交集，使用提供的映射函数来确定相等性。
 */
export function unionBy<T, U>(arr1 : T[], arr2 : T[], mapper : (item : T) => U) : T[] {
	const result : T[] = []
	const seen : Set<U> = new Set()
	for (let i = 0; i < arr1.length; i++) {
		const value = mapper(arr1[i])
		if (!seen.has(value)) {
			seen.add(value)
			result.push(arr1[i])
		}
	}
	for (let i = 0; i < arr2.length; i++) {
		const value = mapper(arr2[i])
		if (!seen.has(value)) {
			seen.add(value)
			result.push(arr2[i])
		}
	}
	return result
}

/**
 * 使用提供的比较函数来确定相等性，从所有给定的数组中创建一个按顺序的唯一值数组。
 * @description 此函数接受两个数组和一个自定义相等性函数，合并这两个数组，并返回一个新数组，该数组仅包含根据自定义相等性函数确定的唯一值。
 * @param arr1 第一个数组
 * @param arr2 第二个数组
 * @param areItemsEqual 一个函数，用于比较两个元素是否相等。该函数接受两个参数，并返回一个布尔值。
 * @returns 一个新的数组，其中包含两个数组的交集，使用提供的比较函数来确定相等性。
 */
export function unionWith<T>(arr1 : T[], arr2 : T[], areItemsEqual : (a : T, b : T) => boolean) : T[] {
	const result : T[] = []
	const seen : T[] = []
	for (let i = 0; i < arr1.length; i++) {
		const value = arr1[i]
		let isDuplicate = false
		for (let j = 0; j < seen.length; j++) {
			if (areItemsEqual(value, seen[j])) {
				isDuplicate = true
				break
			}
		}
		if (!isDuplicate) {
			seen.push(value)
			result.push(value)
		}
	}
	for (let i = 0; i < arr2.length; i++) {
		const value = arr2[i]
		let isDuplicate = false
		for (let j = 0; j < seen.length; j++) {
			if (areItemsEqual(value, seen[j])) {
				isDuplicate = true
				break
			}
		}
		if (!isDuplicate) {
			seen.push(value)
			result.push(value)
		}
	}
	return result
}

/**
 * 返回一个数组，其中包含输入数组 `arr` 中所有元素的唯一值
 * @description 函数接受一个数组作为参数，返回一个新的数组，其中包含输入数组 `arr` 中所有元素的唯一值。
 * @param arr 要查找的数组
 * @returns 一个新的数组，其中包含输入数组 `arr` 中所有元素的唯一值
 */
export function uniq<T>(arr : T[]) : T[] {
	// return Array.from(new Set(arr))
	return [...new Set(arr)]
}

/**
 * 返回一个数组，其中包含输入数组 `arr` 中所有元素的唯一值，并根据提供的 `iteratee` 函数对每个元素进行处理
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含输入数组 `arr` 中所有元素的唯一值，并根据提供的 `iteratee` 函数对每个元素进行处理。
 * @param arr 要查找的数组
 * @param iteratee 一个函数，用于处理每个元素。该函数接受一个参数，并返回一个值
 * @returns 一个新的数组，其中包含输入数组 `arr` 中所有元素的唯一值，并根据提供的 `iteratee` 函数对每个元素进行处理
 */
export function uniqBy<T, K>(arr : T[], iteratee : (item : T) => K) : T[] {
	const result : T[] = []
	const seen : Set<K> = new Set()
	for (let i = 0; i < arr.length; i++) {
		const value = iteratee(arr[i])
		if (!seen.has(value)) {
			seen.add(value)
			result.push(value as T)
		}
	}
	return result
}

/**
 * 返回一个数组，其中包含输入数组 `arr` 中所有元素的唯一值，并根据提供的 `comparator` 函数对每个元素进行比较
 * @description 函数接受一个数组和一个函数作为参数，返回一个新的数组，其中包含输入数组 `arr` 中所有元素的唯一值，并根据提供的 `comparator` 函数对每个元素进行比较。
 * @param arr 要查找的数组
 * @param areItemsEqual 一个函数，用于比较每个元素。该函数接受两个参数，并返回一个数字，表示两者的相对顺序。
 * @returns 一个新的数组，其中包含输入数组 `arr` 中所有元素的唯一值，并根据提供的 `comparator` 函数对每个元素进行比较
 */
export function uniqWith<T>(arr : T[], areItemsEqual : (a : T, b : T) => boolean) : T[] {
	const result : T[] = []
	const seen : T[] = []
	for (let i = 0; i < arr.length; i++) {
		const value = arr[i]
		let isDuplicate = false
		for (let j = 0; j < seen.length; j++) {
			if (areItemsEqual(value, seen[j])) {
				isDuplicate = true
				break
			}
		}
		if (!isDuplicate) {
			seen.push(value)
			result.push(value)
		}
	}
	return result
}

/**
 * 将一个元素组的数组中相同位置的元素收集起来，并将它们作为一个新数组返回。
 * @description 函数接受一个数组作为参数，返回一个新的数组，其中包含 `arr` 数组所有列组成的元素数组。
 * @param arr 要查找的数组
 * @returns 收集同一位置的内部数组元素而创建的新数组。
 */
export function unzip<T>(arr : T[][]) : T[][] {
	const result : T[][] = []
	const length = Math.max(...arr.map((arr) : number => arr.length))
	for (let i = 0; i < length; i++) {
		result.push([])
	}
	for (let i = 0; i < arr.length; i++) {
		for (let j = 0; j < arr[i].length; j++) {
			// result[j][i] = arr[i][j]
			result[j].push(arr[i][j])
		}
	}
	return result
}

/**
 * 将一个嵌套的数组解压缩，并对重新分组的元素应用一个 `iteratee` 函数。
 * @description 函数接受一个数组和一个函数作为参数，实现把 `arr` 数组中元素按照 `iteratee` 函数的返回值进行分组，并返回一个新的数组。
 * @param target 要解压缩的嵌套数组。这是一个数组的数组，其中每个内部数组包含要解压缩的元素。
 * @param iteratee 用于转换解压缩后的元素的函数。
 * @returns 新的解压缩和转换后的元素数组。
 */
function unzipWith<T, R>(target : T[][], iteratee : any) : R[] {
	const result : R[] = []
	const length = Math.max(...target.map((arr) : number => arr.length))
	for (let i = 0; i < length; i++) {
		const args : T[] = []
		for (let j = 0; j < target.length; j++) {
			if (i < target[j].length) {
				args.push(target[j][i])
			}
		}
		result.push((iteratee as ((...args : T[]) => R))?.([...args] as T[]) as R)
	}
	return result
}

/**
 * 创建一个数组，其中排除了所有指定的值。
 * @description 函数接受一个数组和一个或多个值作为参数，返回一个新的数组，其中排除了所有指定的值。使用 `SameValueZero` 进行相等性比较。
 * @param arr 要查找的数组
 * @param values 要排除的值
 * @returns 一个新的数组，其中排除了所有指定的值。
 */
export function without<T>(arr : T[], ...values : T[]) : T[] {
	return arr.filter((value) : boolean => !values.includes(value))
}

/**
 * 计算两个数组之间的对称差集
 * @description 函数接受两个数组作为参数，返回一个新的数组，其中包含两个数组的对称差集。
 * @param arr1 第一个数组
 * @param arr2 第二个数组
 * @returns 一个新的数组，其中包含两个数组的对称差集。
 */
export function xor<T>(arr1 : T[], arr2 : T[]) : T[] {
	return [...new Set([...arr1, ...arr2].filter((value) : boolean => !arr1.includes(value) || !arr2.includes(value)))]
}

/**
 * 使用自定义映射函数来计算两个数组之间的对称差集
 * @description 函数接受两个数组和一个函数作为参数，返回一个新的数组，其中包含两个数组的对称差集，使用提供的映射函数来确定相等性。
 * @param arr1 第一个数组
 * @param arr2 第二个数组
 * @param mapper 一个函数，用于处理每个元素。
 * @returns 一个新的数组，其中包含两个数组的对称差集，使用提供的映射函数来确定相等性。
 */
export function xorBy<T, K>(arr1 : T[], arr2 : T[], mapper : (item : T) => K) : T[] {
	const result : T[] = []
	const seen : Set<K> = new Set()
	for (let i = 0; i < arr1.length; i++) {
		const value = mapper(arr1[i])
		if (!seen.has(value)) {
			seen.add(value)
			if (!arr2.some((item) : boolean => mapper(item) == value)) {
				result.push(arr1[i])
			}
		}
	}
	for (let i = 0; i < arr2.length; i++) {
		const value = mapper(arr2[i])
		if (!seen.has(value)) {
			seen.add(value)
			if (!arr1.some((item) : boolean => mapper(item) == value)) {
				result.push(arr2[i])
			}
		}
	}
	return result
}

/**
 * 使用自定义相等性函数来计算两个数组之间的对称差集
 * @description 函数接受两个数组和一个函数作为参数，返回一个新的数组，其中包含两个数组的对称差集，使用提供的相等性函数来确定相等性。
 * @param arr1 第一个数组
 * @param arr2 第二个数组
 * @param areItemsEqual 一个函数，用于比较两个元素是否相等。该函数接受两个参数，并返回一个布尔值。
 * @returns 一个新的数组，其中包含两个数组的对称差集，使用提供的相等性函数来确定相等性。
 */
export function xorWith<T>(arr1 : T[], arr2 : T[], areItemsEqual : (a : T, b : T) => boolean) : T[] {
	const result : T[] = []
	const seen : T[] = []
	for (let i = 0; i < arr1.length; i++) {
		const value = arr1[i]
		let isDuplicate = false
		for (let j = 0; j < seen.length; j++) {
			if (areItemsEqual(value, seen[j])) {
				isDuplicate = true
				break
			}
		}
		if (!isDuplicate) {
			seen.push(value)
			if (!arr2.some((item) : boolean => areItemsEqual(item, value))) {
				result.push(arr1[i])
			}
		}
	}
	for (let i = 0; i < arr2.length; i++) {
		const value = arr2[i]
		let isDuplicate = false
		for (let j = 0; j < seen.length; j++) {
			if (areItemsEqual(value, seen[j])) {
				isDuplicate = true
				break
			}
		}
		if (!isDuplicate) {
			seen.push(value)
			if (!arr1.some((item) : boolean => areItemsEqual(item, value))) {
				result.push(arr2[i])
			}
		}
	}
	return result
}

/**
 * 将多个数组组合成一个元组数组
 * @description 函数接受多个数组作为参数，返回一个新的数组，其中每一个元素都是一个元组， 包含输入数组的对应元素。如果数组长度不同，结果数组的长度将是最长数组的长度，缺失的值将是 `null`。
 * @param arrs 要组合的数组
 * @returns 一个新的数组，其中包含所有数组的元素。
 */
export function zip<T>(...arrs : T[][]) : T[][] {
	// 找出最长的数组的长度
	const maxLength = Math.max(...arrs.map((arr): number => arr.length));
	// 创建一个新的数组，用于存放最终的元组数组
	const zipped = [] as T[][];

	// 遍历每个位置，直到最长数组的长度
	for (let i = 0; i < maxLength; i++) {
		// 创建一个元组，初始为空数组
		const tuple = [] as T[];
		// 遍历每个输入数组
		for (let j = 0; j < arrs.length; j++) {
			// 如果当前数组有足够的元素，则添加到元组中，否则添加null
			if (i < arrs[j].length) {
				tuple.push(arrs[j][i]);
			} else {
				tuple.push(null as T);
			}
			// tuple.push((arrs[j][i] !=null ? arrs[j][i] : null) as any);
		}
		// 将元组添加到结果数组中
		zipped.push(tuple);
	}

	return zipped;
}

/**
 * 将两个数组合并为一个对象，一个数组包含属性名称，另一个数组包含对应的值。
 * @description 该函数接受两个数组作为输入：一个包含属性名称，另一个包含相应的值。它返回一个新对象，其中第一个数组中的属性名称作为键，第二个数组中对应的元素作为值。如果 `keys` 数组的长度大于 `values` 数组的长度，则返回的对象将包含 `null` 值。
 * @param keys 一个数组
 * @param values 另一个数组
 * @returns 一个新对象，其中第一个数组中的属性名称作为键，第二个数组中对应的元素作为值。
 */
export function zipObject<T>(keys : string[], values : T[]) : UTSJSONObject {
	const result : UTSJSONObject = {}
	for (let i = 0; i < keys.length; i++) {
		// result[keys[i]] = values[i]
		if (i < values.length) {
			result[keys[i]] = values[i]
		} else {
			result[keys[i]] = null
		}
	}
	return result
}

/**
 * 返回数组的第一个元素。该函数接收一个数组作为参数，返回数组的第一个元素。如果数组为空，则返回 `null`。
 * @param arr 要查找的数组
 * @returns 数组的第一个元素。
 */
export function head<T>(arr : T[]) : T | null {
	return arr.length > 0 ? arr[0] : null
}

/**
 * 返回一个新数组，其中除了第一个元素外，包含所有元素。
 * @description 函数接受一个数组作为参数，返回一个新数组，其中除了第一个元素外，包含所有元素。如果数组为空或只有一个元素，则返回一个空数组。
 * @param arr 要查找的数组
 * @returns 一个新数组，其中除了第一个元素外，包含所有元素。
 */
export function tail<T>(arr : T[]) : T[] {
	return arr.length > 1 ? arr.slice(1) : []
}

/**
 * 返回数组的最后一个元素。
 * @description 该函数接收一个数组作为参数，返回数组的最后一个元素。如果数组为空，则返回 `null`。
 * @param arr 要查找的数组
 * @returns 数组的最后一个元素。
 */
export function last<T>(arr : T[]) : T | null {
	return arr.length > 0 ? arr[arr.length - 1] : null
}

/**
 * 返回一个数组，其中包含数组中除最后一个元素外的所有元素。
 * @description 函数接受一个数组作为参数，返回一个新数组，其中包含数组中除最后一个元素外的所有元素。如果数组为空或只有一个元素，则返回一个空数组。
 * @param arr 要查找的数组
 * @returns 一个数组，其中包含数组中除最后一个元素外的所有元素。
 */
export function initial<T>(arr : T[]) : T[] {
	return arr.length > 1 ? arr.slice(0, -1) : []
}

/**
 * 从数组中查找第一个元素满足条件的索引
 * @description 函数接受一个数组和一个函数作为参数，返回第一个元素满足条件的索引。
 * @param arr 要查找的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。该函数接受一个参数，并返回一个布尔值
 * @returns 第一个元素满足条件的索引。
 */
export function findIndex<T>(arr : T[], predicate : (item : T) => boolean) : number {
	for (let i = 0; i < arr.length; i++) {
		if (predicate(arr[i])) {
			return i
		}
	}
	return -1
}

/**
 * 从数组中查找第一个元素满足条件的元素
 * @description 函数接受一个数组和一个函数作为参数，返回第一个元素满足条件的元素。
 * @param arr 要查找的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。该函数接受一个参数，并返回一个布尔值
 * @returns 第一个元素满足条件的元素。
 */
export function find<T>(arr : T[], predicate : (item : T) => boolean) : T | null {
	for (let i = 0; i < arr.length; i++) {
		if (predicate(arr[i])) {
			return arr[i]
		}
	}
	return null
}

/**
 * 从数组中查找所有元素满足条件的索引
 * @description 函数接受一个数组和一个函数作为参数，返回所有元素满足条件的索引。
 * @param arr 要查找的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。该函数接受一个参数，并返回一个布尔值
 * @returns 所有元素满足条件的索引。
 */
export function findIndices<T>(arr : T[], predicate : (item : T) => boolean) : number[] {
	const result : number[] = []
	for (let i = 0; i < arr.length; i++) {
		if (predicate(arr[i])) {
			result.push(i)
		}
	}
	return result
}

/**
 * 从数组中查找所有元素满足条件的元素
 * @description 函数接受一个数组和一个函数作为参数，返回所有元素满足条件的元素。
 * @param arr 要查找的数组
 * @param predicate 一个函数，用于判断元素是否满足条件。该函数接受一个参数，并返回一个布尔值
 * @returns 所有元素满足条件的元素。
 */
export function filter<T>(arr : T[], predicate : (item : T) => boolean) : T[] {
	const result : T[] = []
	for (let i = 0; i < arr.length; i++) {
		if (predicate(arr[i])) {
			result.push(arr[i])
		}
	}
	return result
}

/**
 * 从数组中查找所有元素不满足条件的元素
 * @description 函数接受一个数组和一个函数作为参数，返回所有元素不满足条件的元素。
 * @param arr 要查找的数组
 * @param predicate 一个函数，用于判断元素是否不满足条件。该函数接受一个参数，并返回一个布尔值
 * @returns 所有元素不满足条件的元素。
 */
export function reject<T>(arr : T[], predicate : (item : T) => boolean) : T[] {
	const result : T[] = []
	for (let i = 0; i < arr.length; i++) {
		if (!predicate(arr[i])) {
			result.push(arr[i])
		}
	}
	return result
}

/**
 * 从数组中检索指定索引处的元素。
 * @description 函数接受一个数组和一个索引数组作为参数，返回一个新数组，其中包含数组中指定索引处的元素。支持负数索引，即从数组末尾开始计数。
 * @param arr 要查找的数组
 * @param indices 一个数组，包含要检索的索引
 * @returns 一个新数组，其中包含数组中指定索引处的元素。
 */
export function at<T>(arr : T[], indices : number[]) : (T | null)[] {
	const result : (T | null)[] = []
	for (let i = 0; i < indices.length; i++) {
		const index = indices[i]
		// 判断是否超出范围，超出范围则返回null
		if (index >= arr.length) {
			result.push(null);
		} else {
			result.push(arr[index >= 0 ? index : arr.length + index])
		}
	}
	return result
}

/**
 * 如果值不是数组，则将其转换为数组。
 * @description 函数接受一个值作为参数，如果值不是数组，则将其转换为数组。如果值是数组，则返回原值。
 * @param value 要转换的值
 * @returns 转换后的数组。
 */
export function castArray<T>(value ?: any) : T | null[] {
	if (value == null) {
		return [null]
	}
	return Array.isArray(value as any) ? value as T | null[] : [value as T] as T | null[]
}