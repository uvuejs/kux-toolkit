## 1.0.4（2025-03-25）
+ 【重要】新增支持微信小程序。
+ 新增 `at` 数组工具函数，用于获取数组中的元素。[文档](https://toolkit.uvuejs.cn/reference/array/at.html)
+ 新增 `castArray` 数组工具函数，用于将非数组的值转换成数组。[文档](https://toolkit.uvuejs.cn/reference/array/castArray.html)
+ 新增 `before` 函数工具函数，用于限制给定函数被调用的次数。[文档](https://toolkit.uvuejs.cn/reference/function/before.html)
+ 新增 `after` 函数工具函数，用于指定给定函数在限制次数后开始调用。[文档](https://toolkit.uvuejs.cn/reference/function/after.html)
+ 新增 `asyncNoop` 函数工具函数，用于创建一个不做任何事情的异步函数。[文档](https://toolkit.uvuejs.cn/reference/function/asyncNoop.html)
+ 新增 `debounce` 函数工具函数，用于防抖动。[文档](https://toolkit.uvuejs.cn/reference/function/debounce.html)
+ 新增 `retry` 函数工具函数，用于重试函数。[文档](https://toolkit.uvuejs.cn/reference/function/retry.html)
+ 新增 `torottle` 函数工具函数，用于节流函数。[文档](https://toolkit.uvuejs.cn/reference/function/throttle.html)
+ 新增 `add` 数学工具函数，用于两个数相加求和。[文档](https://toolkit.uvuejs.cn/reference/math/add.html)
+ 新增 `ceil` 数学工具函数，用于将一个数字向上取整到指定的精度。[文档](https://toolkit.uvuejs.cn/reference/math/ceil.html)
+ 新增 `divide` 数学工具函数，用于两个数相除。[文档](https://toolkit.uvuejs.cn/reference/math/divide.html)
+ 新增 `floor` 数学工具函数，用于将一个数字向下取整到指定的精度。[文档](https://toolkit.uvuejs.cn/reference/math/floor.html)
+ 新增 `multiply` 数学工具函数，用于两个数相乘。[文档](https://toolkit.uvuejs.cn/reference/math/multiply.html)
+ 新增 `subtract` 数学工具函数，用于两个数相减。[文档](https://toolkit.uvuejs.cn/reference/math/subtract.html)
+ 新增 `camelCase` 字符串工具函数，用于将字符串转换成驼峰形式。[文档](https://toolkit.uvuejs.cn/reference/string/camelCase.html)
+ 新增 `capitalize` 字符串工具函数，用于将字符串的首字母大写。[文档](https://toolkit.uvuejs.cn/reference/string/capitalize.html)
+ 新增 `constantCase` 字符串工具函数，用于将字符串转换成常量形式。[文档](https://toolkit.uvuejs.cn/reference/string/constantCase.html)
+ 新增 `endsWith` 字符串工具函数，用于检查字符串是否在其末尾包含另一个字符串。[文档](https://toolkit.uvuejs.cn/reference/string/endsWith.html)
+ 新增 `kebabCase` 字符串工具函数，用于将字符串转换成短横线形式。[文档](https://toolkit.uvuejs.cn/reference/string/kebabCase.html)
+ 新增 `lowerCase` 字符串工具函数，用于将字符串转换成小写。[文档](https://toolkit.uvuejs.cn/reference/string/lowerCase.html)

+ 【版本注意】该版本需要在 `4.55`及以上编译器版本中使用。
## 1.0.3（2024-10-25）
+ 修复 `4.31版本编译器` 编译失败的问题。

## 1.0.2（2024-08-22）
+ 新增 `clamp` 数学工具函数，用于限制数值在指定范围内。[文档](https://toolkit.uvuejs.cn/reference/math/clamp.html)
+ 新增 `inRange` 数学工具函数，用于判断数值是否在指定范围内。[文档](https://toolkit.uvuejs.cn/reference/math/inRange.html)
+ 新增 `mean` 数学工具函数，用于计算数组的平均值。[文档](https://toolkit.uvuejs.cn/reference/math/mean.html)
+ 新增 `meanBy` 数学工具函数，用于计算数组的平均值，根据回调函数获取值。[文档](https://toolkit.uvuejs.cn/reference/math/meanBy.html)
+ 新增 `random` 数学工具函数，用于生成随机数。[文档](https://toolkit.uvuejs.cn/reference/math/random.html)
+ 新增 `randomInt` 数学工具函数，用于生成随机整数。[文档](https://toolkit.uvuejs.cn/reference/math/randomInt.html)
+ 新增 `range` 数学工具函数，根据起始值、结束值和步长生成数组。[文档](https://toolkit.uvuejs.cn/reference/math/range.html)
+ 新增 `round` 数学工具函数，用于四舍五入。[文档](https://toolkit.uvuejs.cn/reference/math/round.html)
+ 新增 `sum` 数学工具函数，用于计算数组的总和。[文档](https://toolkit.uvuejs.cn/reference/math/sum.html)
+ 新增 `sumBy` 数学工具函数，用于计算数组的总和，根据回调函数获取值。[文档](https://toolkit.uvuejs.cn/reference/math/sumBy.html)

## 1.0.1（2024-08-16）
+ 新增 `sample` 数组工具函数，用于生成指定长度的随机数组。[文档](https://https://toolkit.uvuejs.cn/reference/array/sample.html)
+ 新增 `sampleSize` 数组工具函数，用于从数组中随机选取指定数量的元素。[文档](https://toolkit.uvuejs.cn/reference/array/sampleSize.html)
+ 新增 `shuffle` 数组工具函数，用于打乱数组顺序。[文档](https://toolkit.uvuejs.cn/reference/array/shuffle.html)
+ 新增 `size` 数组工具函数，用于获取数组、对象、字符串的长度。[文档](https://toolkit.uvuejs.cn/reference/array/size.html)
+ 新增 `take` 数组工具函数，用于从数组中取出前 n 个元素。[文档](https://toolkit.uvuejs.cn/reference/array/take.html)
+ 新增 `takeWhile` 数组工具函数，用于从数组中取出满足条件的元素。[文档](https://toolkit.uvuejs.cn/reference/array/takeWhile.html)
+ 新增 `takeRight` 数组工具函数，用于从数组中取出后 n 个元素。[文档](https://toolkit.uvuejs.cn/reference/array/takeRight.html)
+ 新增 `takeRightWhile` 数组工具函数，用于从数组中取出后面满足条件的元素。[文档](https://toolkit.uvuejs.cn/reference/array/takeRightWhile.html)
+ 新增 `union` 数组工具函数，用于合并两个数组。[文档](https://toolkit.uvuejs.cn/reference/array/union.html)
+ 新增 `unionBy` 数组工具函数，用于合并两个数组，根据回调函数判断是否合并。[文档](https://toolkit.uvuejs.cn/reference/array/unionBy.html)
+ 新增 `unionWith` 数组工具函数，用于合并两个数组，根据回调函数判断是否合并。[文档](https://toolkit.uvuejs.cn/reference/array/unionWith.html)
+ 新增 `uniq` 数组工具函数，用于过滤数组中的重复元素。[文档](https://toolkit.uvuejs.cn/reference/array/uniq.html)
+ 新增 `uniqBy` 数组工具函数，用于过滤数组中的重复元素，根据回调函数判断是否重复。[文档](https://toolkit.uvuejs.cn/reference/array/uniqBy.html)
+ 新增 `uniqWith` 数组工具函数，用于过滤数组中的重复元素，根据回调函数判断是否重复。[文档](https://toolkit.uvuejs.cn/reference/array/uniqWith.html)
+ 新增 `unzip` 数组工具函数，用于将数组的元素打包成两个数组。[文档](https://toolkit.uvuejs.cn/reference/array/unzip.html)
+ 新增 `without` 数组工具函数，用于过滤数组中指定元素。[文档](https://toolkit.uvuejs.cn/reference/array/without.html)
+ 新增 `xor` 数组工具函数，用于两个数组的异或运算。[文档](https://toolkit.uvuejs.cn/reference/array/xor.html)
+ 新增 `xorBy` 数组工具函数，用于两个数组的异或运算，根据回调函数判断是否异或。[文档](https://toolkit.uvuejs.cn/reference/array/xorBy.html)
+ 新增 `xorWith` 数组工具函数，用于两个数组的异或运算，根据回调函数判断是否异或。[文档](https://toolkit.uvuejs.cn/reference/array/xorWith.html)
+ 新增 `zip` 数组工具函数，用于将多个数组打包成一个数组。[文档](https://toolkit.uvuejs.cn/reference/array/zip.html)
+ 新增 `zipObject` 数组工具函数，用于将键值对转换成对象。[文档](https://toolkit.uvuejs.cn/reference/array/zipObject.html)
+ 新增 `head` 数组工具函数，用于获取数组的第一个元素。[文档](https://toolkit.uvuejs.cn/reference/array/head.html)
+ 新增 `tail` 数组工具函数，返回一个新数组，其中除了第一个元素外，包含所有元素。[文档](https://toolkit.uvuejs.cn/reference/array/tail.html)
+ 新增 `last` 数组工具函数，用于获取数组的最后一个元素。[文档](https://toolkit.uvuejs.cn/reference/array/last.html)
+ 新增 `initial` 数组工具函数，用于获取数组的除最后一个元素外的所有元素。[文档](https://toolkit.uvuejs.cn/reference/array/initial.html)

## 1.0.0（2024-08-09）
+ 第一个版本发布
