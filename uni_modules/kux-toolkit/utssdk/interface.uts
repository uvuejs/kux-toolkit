// import { KuxAbortSignal } from '../../kux-abort-signal/utssdk/interface';
import { KuxAbortSignal } from '@/uni_modules/kux-abort-signal';

/**
 * 将一个数组分割成指定长度的多个子数组
 * @description 函数接受一个数组和一个数字作为参数，返回一个新的数组，其中包含原数组分割成指定长度的多个子数组。
 * @param arr 要分割的数组
 * @param size 子数组的长度
 * @returns 一个新的数组，其中包含原数组分割成指定长度的多个子数组。
 */
// export declare function chunk<T>(arr: T[], size: number): T[][]

/**
 * 将多个数组合并成一个数组
 * @description 函数接受多个数组作为参数，返回一个新的数组，其中包含所有传入的数组的元素。
 * @param arrs 要合并的数组
 * @returns 一个新的数组，其中包含所有传入的数组的元素。
 */
// export declare function concat<T>(...arrs: T[]): T[]

// #ifndef APP-ANDROID && !WEB
export type AbortEventListener = () => void;
export class AbortSignal {
	private _aborted: boolean = false;
	private listeners: AbortEventListener[] = [];
	
	get aborted(): boolean {
		return this._aborted;
	}
	
	addEventListener(eventName: string, listener: AbortEventListener): void {
		if (typeof listener !== "function") {
		    throw new Error("Listener must be a function");
		  }
		if (this._aborted) {
			listener();
		} else {
			this.listeners.push(listener);
		}
	}
	
	removeEventListener(listener: AbortEventListener): void {
		const index = this.listeners.indexOf(listener);
		if (index !== -1) {
			this.listeners.splice(index, 1);
		}
	}
	
	triggerAbort(): void {
		if (!this._aborted) {
			this._aborted = true;
			this.listeners.forEach(listener => listener());
			this.listeners = [];
		}
	}
	
	static timeout(ms: number): AbortSignal {
		const signal = new AbortSignal();
		setTimeout(() => signal.triggerAbort(), ms);
		return signal;
	}
}
// #endif

export type DebounceOptions = {
	signal?: KuxAbortSignal;
	edges?: Array<'leading' | 'trailing'>;
}

export type RetryOptions = {
	retries: number;
	delay?: number;
	// #ifdef APP-ANDROID
	signal?: KuxAbortSignal;
	// #endif
	// #ifndef APP-ANDROID
	signal?: typeof KuxAbortSignal;
	// #endif
}

export type ThrottleOptions = {
	signal?: KuxAbortSignal;
	edges?: Array<'leading' | 'trailing'>;
}

export interface IDebouncedFunction {
	(...args: any[]): void;
	cancel: () => void;
	flush: () => void;
	schedule: () => void;
}

export interface IThrottledFunction {
	(...args: any[]): void;
	cancel: () => void;
	flush: () => void;
}

// #ifdef APP-ANDROID
/**
 * 创建一个防抖函数，它会延迟调用提供的函数，直到距离上次调用已过去 `debounceMs` 毫秒为止。
 * @description 防抖函数还具有一个 cancel 方法，用于取消任何待定的执行。
 * @param {Function} func 要防抖的函数。
 * @param {Number} debounceMs 防抖时间。
 * @param {DebounceOptions} options 附加选项。
 * @returns {Function} 新的函数。
 */
export declare function debounce<F extends (...args: any[]) => void>(
	func: F,
	debounceMs: number,
	options?: DebounceOptions
): IDebouncedFunction;

/**
 * 创建一个节流函数，它会在 `throttleMs` 毫秒内限制调用提供的函数一次。
 * @description 节流函数还具有一个 cancel 方法，用于取消任何待定的执行。
 * @param {Function} func 要节流的函数。
 * @param {Number} throttleMs 节流时间。
 * @param {ThrottleOptions} options 附加选项。
 * @returns {Function} 新的函数。
 */
export declare function throttle<F extends (...args: any[]) => void>(
	func: F, 
	throttleMs: number, 
	options?: ThrottleOptions
): IThrottledFunction;

/**
 * 创建一个函数，限制只能调用提供的函数 `func` 一次。
 * @description 对该函数的重复调用会返回第一次调用时的结果。
 */
export declare function once<F extends (...args: any[]) => any>(func: F): F;

/**
 * 将一个数字向上舍入到指定的精度。
 * @description 该函数接受一个数字和一个可选的精度值，并返回该数字向上舍入到指定的小数位数的结果。
 * @param {number|string} number 要向上舍入的数字
 * @param {number|string} precision 精度值 (默认为 `0`)
 * @returns {number} 向上舍入后的数字
 */
export declare function ceil(number: number | string, precision: number | string): number;

/**
 * 将一个数字向下舍入到指定的精度。
 * @description 该函数接受一个数字和一个可选的精度值，返回将数字向下舍入到指定小数位数的结果。
 * @param {number|string} number 要向下舍入的数字
 * @param {number|string} precision 精度值 (默认为 `0`)
 * @returns {number} 向下舍入后的数字
 */
export declare function floor(number: number | string, precision: number | string): number;

/**
 * 将一个数字四舍五入到指定的精度。
 * @description 该函数接受一个数字和一个可选的精度值，返回该数字四舍五入到指定的小数位数的结果。
 * @param {number|string} number 要四舍五入的数字
 * @param {number|string} precision 精度值 (默认为 `0`)
 * @returns {number} 四舍五入后的数字
 */
export declare function round(number: number | string, precision: number | string): number;

/**
 * 将一个字符串转换为驼峰式命名。
 * @description 该函数接受一个字符串，并返回该字符串的驼峰式命名版本。
 * @param {string} str 要转换的字符串
 * @returns {string} 驼峰式命名的字符串
 */
export declare function camelCase(str: string): string;

/**
 * 将字符串的第一个字符转为大写，其余字符转为小写。
 * @description 该函数接受一个字符串，并返回该字符串的首字母大写，其余字母小写的版本。
 * @param {string} str 要转换的字符串
 * @returns {string} 首字母大写，其余字母小写的字符串
 */
export declare function capitalize(str: string): string;

/**
 * 将字符串转换为常量式命名。如：`some_string` 转换为 `SOME_STRING`。
 * @description 该函数接受一个字符串，并返回该字符串的常量式命名版本。
 * @param {string} str 要转换的字符串
 * @returns {string} 常量式命名的字符串
 */
export declare function constantCase(str: string): string;

/**
 * 检查字符串是否在其末尾包含另一个字符串。
 * @description 该函数接受一个字符串和另一个字符串，并返回一个布尔值，指示字符串是否在其末尾包含另一个字符串。
 * @param {string} str 要检查的字符串
 * @param {string} target 要搜索的字符串
 * @returns {boolean} 布尔值，指示字符串是否在其末尾包含另一个字符串
 */
export declare function endsWith(str: string, target: string, position?: number): string;

/**
 * 将字符串转换为短横线连接的小写格式。
 * @description 该函数接受一个字符串，并返回该字符串的短横线连接的小写格式。如：'kebab-case'
 * @param {string} str 要转换的字符串
 * @returns {string} 短横线连接的小写格式的字符串
 */
export declare function kebabCase(str: string): string;

/**
 * 将字符串转换为小写格式。
 * @description 该函数接受一个字符串，并返回小写格式，如：`lower case`。
 * @param {string} str 要转换的字符串
 * @returns {string} 小写格式的字符串
 */
export declare function lowerCase(str: string): string;
// #endif