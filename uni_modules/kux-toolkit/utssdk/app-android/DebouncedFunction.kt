package uts.sdk.modules.kuxToolkit
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.coroutines.CoroutineContext
import java.util.concurrent.atomic.AtomicBoolean
import uts.sdk.modules.kuxAbortSignal.*

// data class DebounceOptions(
//     val signal: KuxAbortSignal? = null,
//     val edges: MutableList<String> = mutableListOf("trailing")
// )

class DebouncedFunction<Args>(
    private val func: suspend (Args) -> Unit,
    private val debounceMs: Long,
    private val options: DebounceOptions,
    private val coroutineScope: CoroutineScope
) {
    private var leadingJob: Job? = null
    private var trailingJob: Job? = null
    private var lastArgs: Args? = null
	private val edges: MutableList<String> = mutableListOf("trailing")
    private val mutex = Mutex()
    private val scheduleChannel = Channel<Unit>(Channel.UNLIMITED)

    init {
        // options.signal?.addEventListener { cancel() }
		options.signal?.addEventListener("abort") {
			cancel()
		}
		if (options.edges?.size == 1) {
			options.edges?.contains("leading")?.let {
				if (it) {
					edges.remove("trailing")
				} else {
					edges.remove("leading")
				}
			}
		}
        startScheduler()
    }

    private fun startScheduler() = coroutineScope.launch {
        for (unit in scheduleChannel) {
            handleCall()
        }
    }

    operator fun invoke(args: Args) {
		coroutineScope.launch {
            mutex.withLock {
                lastArgs = args
                scheduleChannel.send(Unit)
            }
        }
    }

    private suspend fun handleCall() = mutex.withLock {
        // Leading 边缘处理
        if (edges.contains("leading") && leadingJob?.isActive != true) {
            leadingJob?.cancel()
            leadingJob = launchLeading()
        }

        // Trailing 边缘处理
        trailingJob?.cancel()
        if (edges.contains("trailing")) {
            trailingJob = launchTrailing()
        }
    }

    private fun launchLeading(): Job = coroutineScope.launch {
        lastArgs?.let { args ->
            func(args)
            delay(debounceMs) // Leading 调用后需要冷却时间
        }
    }

    private fun launchTrailing(): Job = coroutineScope.launch {
        delay(debounceMs)
        mutex.withLock {
            lastArgs?.let { args ->
                func(args)
                lastArgs = null
            }
        }
    }

    fun cancel() {
		coroutineScope.launch {
            mutex.withLock {
                leadingJob?.cancel()
                trailingJob?.cancel()
                lastArgs = null
            }
        }
    }

    fun flush() {
		coroutineScope.launch {
            mutex.withLock {
                trailingJob?.cancel()
                lastArgs?.let { args ->
                    func(args)
                    lastArgs = null
                }
            }
        }
    }

    fun schedule() {
        coroutineScope.launch {
            mutex.withLock {
                if (lastArgs != null) {
                    scheduleChannel.send(Unit)
                }
            }
        }
    }
}

fun <Args> debounce(
    func: suspend (Args) -> Unit,
    debounceMs: Long,
    options: DebounceOptions = DebounceOptions(),
    coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
): DebouncedFunction<Args> {
    return DebouncedFunction(func, debounceMs, options, coroutineScope)
}