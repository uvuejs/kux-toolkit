package uts.sdk.modules.kuxToolkit
import java.math.BigDecimal
import java.math.RoundingMode
import io.dcloud.uts.console

fun <R> once(func: () -> R): () -> R {
    var result: R? = null
    return {
        result ?: func().also { result = it }
    }
}

fun <A, R> once(func: (A) -> R): (A) -> R {
    var result: R? = null
    return { a ->
        result ?: func(a).also { result = it }
    }
}

fun <A, B, R> once(func: (A, B) -> R): (A, B) -> R {
    var result: R? = null
    return { a, b ->
        result ?: func(a, b).also { result = it }
    }
}

// 以此类推支持更多参数...

// 数字解析扩展函数
private fun Any.parseToBigDecimal(argName: String): BigDecimal = when (this) {
	is String -> try {
		BigDecimal(this)
	} catch (e: NumberFormatException) {
		throw IllegalArgumentException("【kux-toolkit:parseToBigDecimal】$argName must be a valid number string")
	}
	is Number -> BigDecimal(this.toString())
	else -> throw IllegalArgumentException("【kux-toolkit:parseToBigDecimal】$argName must be a String or Number")
}

// 精度解析扩展函数
private fun Any.parseToPrecision(): Int = when (this) {
	is String -> this.toIntOrNull() 
		?: throw IllegalArgumentException("【kux-toolkit:parseToPrecision】Precision must be integer")
	is Number -> {
		if (this.toDouble() % 1 != 0.0) throw IllegalArgumentException("【kux-toolkit:parseToPrecision】Precision must be integer")
		this.toInt()
	}
	else -> throw IllegalArgumentException("【kux-toolkit:parseToPrecision】Precision must be integer")
}

private fun roundCore(
	number: Any,
	precision: Any,
	mode: RoundingMode
): BigDecimal {
	// 参数解析
	val num = number.parseToBigDecimal("number")
	val pre = precision.parseToPrecision()
	
	// 精度计算
	val absPrecision = Math.abs(pre)
	val tenPower = BigDecimal.TEN.pow(absPrecision)
	
	// 计算缩放因子
	val factor = if (pre >= 0) {
		tenPower
	} else {
		BigDecimal.ONE.divide(tenPower, absPrecision, RoundingMode.UNNECESSARY)
	}
	
	// 缩放+取整+还原
	return num.multiply(factor)
		.setScale(0, mode)
		.divide(factor)
		.setScale(
			Math.max(pre, 0),
			RoundingMode.UNNECESSARY
		)
}

fun ceil(number: Any, precision: Any = 0) = roundCore(number, precision, RoundingMode.CEILING)

fun round(number: Any, precision: Any = 0) = roundCore(number, precision, RoundingMode.HALF_UP)

fun floor(number: Any, precision: Any = 0) = roundCore(number, precision, RoundingMode.FLOOR)

fun camelCase(str: String): String {
	if (str.isEmpty()) return ""
	
	// 分割逻辑：匹配分隔符或大小写转换点
	val regex = Regex(
		"[_\\-\\s]+|" +          // 匹配分隔符（下划线、连字符、空格）
		"(?<=[a-z])(?=[A-Z])|" + // 小写后接大写（如 camelCase → camel|Case）
		"(?<=[A-Z])(?=[A-Z][a-z])" // 连续大写后接小写（如 HTTPRequest → HTTP|Request）
	)
	
	return str.split(regex)
		.filter { it.isNotEmpty() }
		.mapIndexed { index, s ->
			when (index) {
				// 首字母全小写
				0 -> s.toLowerCase()
				// 其他字母首字母大写
				else -> s.toLowerCase()
					.replaceFirstChar(Char::titlecase)
			}
		}
		.joinToString("")
}

fun capitalize(input: String): String {
	return when {
        input.isEmpty() -> input
        input.length == 1 -> input.uppercase()
        input[0].isUpperCase() && input.substring(1).all { it.isLowerCase() } -> input
        else -> input.lowercase().replaceFirstChar { it.uppercaseChar() }
    }
}

fun constantCase(input: String): String {
	return input
		.replace(Regex("[\\s-]+"), "_")
		.replace(Regex(
			"(?<=[a-z])(?=[A-Z])|" +     // 小写接大写
			"(?<=[A-Z])(?=[A-Z][a-z])|" +// 连续大写后接小写
			"(?<=[a-zA-Z])(?=\\d)|" +    // 字母接数字
			"(?<=\\d)(?=[a-zA-Z])"       // 数字接字母
		), "_")
		.uppercase()
		.replace(Regex("[_]+"), "_")
		.replace(Regex("^_|_$"), "")
}

fun endsWith(str: String, target: String, position: Int = str.length): Boolean {
	// 边界条件处理
	if (target.isEmpty()) return true  // 空字符串始终匹配
	if (position < 0 || position > str.length) return false
	if (target.length > position) return false
	
	// 计算截取起始位置
	val startIndex = position - target.length
	if (startIndex < 0) return false
	
	// 截取并比较
	return str.substring(startIndex, position) == target
}

private val CAMEL_REGEX = Regex("([a-z])([A-Z])")
private val BIG_CAMEL_REGEX = Regex("([A-Z]{2,})([A-Z][a-z])")
private val SEPARATORS_REGEX = Regex("[\\s_]+")
private val CLEAN_REGEX = Regex("[^a-z0-9-]")
private val DASHES_REGEX = Regex("-+")
private val TRIM_REGEX = Regex("^-|-$")

fun kebabCase(str: String): String {
	return str
		.replace(CAMEL_REGEX) { "${it.groupValues[1]}-${it.groupValues[2]}" }
		.replace(BIG_CAMEL_REGEX) { "${it.groupValues[1]}-${it.groupValues[2]}" }
		.replace(SEPARATORS_REGEX, "-")
		.lowercase()
		.replace(CLEAN_REGEX, "")
		.replace(DASHES_REGEX, "-")
		.replace(TRIM_REGEX, "")
}

// 使用伴生对象存储预编译正则
class StringUtils {
    companion object {
        private val DIGIT_BOUNDARY_REGEX = Regex("(\\d)([A-Za-z])")
        private val SEPARATORS_REGEX = Regex("[_-]")
        private val BIG_CAMEL_REGEX = Regex("([A-Z])([A-Z][a-z])")
        private val CAMEL_REGEX = Regex("([a-z])([A-Z])")
        private val SPACE_REGEX = Regex("\\s+")
    }

    fun toLowerCase(str: String): String {
        return str
            .replace(DIGIT_BOUNDARY_REGEX, "$1 $2")
            .replace(SEPARATORS_REGEX, " ")
            .replace(BIG_CAMEL_REGEX) { "${it.groupValues[1]} ${it.groupValues[2]}" }
            .replace(CAMEL_REGEX) { "${it.groupValues[1]} ${it.groupValues[2]}" }
            .lowercase()
            .replace(SPACE_REGEX, " ")
            .trim()
    }
}

fun lowerCase(str: String): String {
	return StringUtils().toLowerCase(str)
}