package uts.sdk.modules.kuxToolkit

import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import uts.sdk.modules.kuxAbortSignal.*
import io.dcloud.uts.console

// data class ThrottleOptions(
// 	val signal: KuxAbortSignal? = null,
// 	val edges: List<String> = listOf("trailing")
// )

class ThrottledFunction<Args>(
	private val func: suspend (Args) -> Unit,
	private val throttleMs: Long,
	private val options: ThrottleOptions,
	private val coroutineScope: CoroutineScope
) {
	private var pendingArgs: Args? = null
	private var coolingDown: Boolean = false
	private val mutex = Mutex()
	private val edges: MutableList<String> = mutableListOf("leading", "trailing")
	private var trailingJob: Job? = null
	
	init {
		options.signal?.addEventListener("abort") { cancel() }
		options.edges?.let { edges ->
			this.edges.clear()
			this.edges.addAll(edges)
		}
	}
	
	operator fun invoke(args: Args) {
		coroutineScope.launch {
			mutex.withLock {
				pendingArgs = args
				if (!coolingDown) {
					// 检查是否需要触发 leading 边缘
					edges.contains("leading").let {
						if (it) {
							executePending()
						}
					}
					startCoolingDown()
				}
			}
		}
	}
	
	private fun startCoolingDown() {
		coolingDown = true
		// trailingJob?.cancel()
		trailingJob = coroutineScope.launch {
			delay(throttleMs)
			mutex.withLock {
				coolingDown = false
				edges.contains("trailing").let {
					if (it) {
						executePending()
					}
				}
			}
		}
	}
	
	private suspend fun executePending() {
		pendingArgs?.let { args ->
			func(args)
			pendingArgs = null
		}
	}
	
	fun cancel() {
		coroutineScope.launch {
			mutex.withLock {
				coolingDown = false
				trailingJob?.cancel()
				pendingArgs = null
			}
		}
	}
	
	fun flush() {
		coroutineScope.launch {
			mutex.withLock {
				if (coolingDown) {
					trailingJob?.cancel()
					coolingDown = false
					edges.contains("trailing").let {
						if (it) {
							executePending()
						}
					}
				}
			}
		}
	}
}

fun <Args> throttle(
	func: suspend (Args) -> Unit,
	throttleMs: Long,
	options: ThrottleOptions = ThrottleOptions(),
	coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
): ThrottledFunction<Args> {
	return ThrottledFunction(func, throttleMs, options, coroutineScope)
}