export type AbortEventListener = () => void;

export declare class KuxAbortSignal {
	/** 表示信号是否已被终止 */
	readonly aborted: boolean;
	
	/**
	 * 添加事件监听器
	 * @param eventName - 事件名称（仅支持 "abort"）
	 * @param listener - 回调函数
	 */
	addEventListener(eventName: string, listener: AbortEventListener): void;
	
	/**
	 * 移除事件监听器
	 * @param eventName - 事件名称（仅支持 "abort"）
	 * @param listener - 回调函数
	 */
	removeEventListener(eventName: string, listener: AbortEventListener): void;
	
	/** @internal 内部方法，触发中止事件 */
	triggerAbort(): void;
	
	/**
	 * 创建超时自动中止的信号
	 * @param ms - 超时时间（毫秒）
	 */
	static timeout(ms: number): KuxAbortSignal;
}

export declare class KuxAbortController {
	/** 信号对象 */
	readonly signal: KuxAbortSignal;
	
	/**
	 * 中止信号
	 */
	abort(): void;
}