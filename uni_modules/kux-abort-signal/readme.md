# kux-abort-signal

模拟实现 [AbortSignal](https://developer.mozilla.org/zh-CN/docs/Web/API/AbortSignal) 信号对象，允许通过 `KuxAbortController` 对象与异步操作进行通信并在需要时将其中止。

## KuxAbortSignal 对象

### 属性

#### aborted `只读`
一个 `Boolean` 值，表示与之通信的请求是否被中止（`true`）或未中止（`false`）。

#### reason `只读`
一旦信号被中止，提供一个使用任意值表示的中止原因。

### 静态方法

#### timeout
返回一个在指定时间后自动中止的 `KuxAbortSignal` 实例。

### 实例方法

#### addEventListener
添加事件监听器，一般添加 `abort` 事件类型。

#### removeEventListener
移除事件监听器。

## KuxAbortController 对象

### 属性

#### signal `只读`
返回一个 `KuxAbortSignal` 对象实例，可以用它来和异步操作进行通信或者中止这个操作。

### 实例方法

#### abort
中止一个尚未完成的异步操作。这能够中止任何响应体和流的使用。

## 示例

```ts
import { KuxAbortSignal, KuxAbortController } from '@/uni_modules/kux-abort-signal';

const controller = new KuxAbortController();
const signal = controller.signal;

// 添加监听器
signal.addEventListener("abort", () => console.log("Aborted!"));
signal.addEventListener("custom-event", () => console.log("Custom event"));

// 移除监听器
const listener = () => console.log("Another listener");
signal.addEventListener("abort", listener);
signal.removeEventListener("abort", listener); // 精准移除

controller.abort(); // 输出 "Aborted!"，并清空所有监听器
```

---
### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

___
### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手